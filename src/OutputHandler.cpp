/**
 * Copyright 2020, Tuukka Norri <tsnorri@iki.fi>
 * 
 * This file is part of CHIC aligner.
 * 
 * CHIC aligner is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * CHIC aligner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with CHIC aligner.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <boost/format.hpp>
#include <iostream>
#include "OutputHandler.h"
#include "SamRecord.h"


void StdoutOutputHandler::prepare()
{
	for (auto const &header : m_sam_headers)
		std::cout << header << '\n';
}


void StdoutOutputHandler::output_record(SamRecord const &occ)
{
	// Filter if needed.
	if (0 == occ.getPos())
		return;

	if ("*" == occ.getRname())
		return;

	if (m_max_edit_distance < occ.getEditDistance())
		return;
	
	std::cout << occ << '\n';
}


void StdoutOutputHandler::finish()
{
	std::cout << std::flush;
}


void SplittingBamOutputHandler::output_record(SamRecord const &occ)
{
	// Filter if needed.
	if (0 == occ.getPos())
		return;

	if ("*" == occ.getRname())
		return;

	if (m_max_edit_distance < occ.getEditDistance())
		return;
	
	// Find the correct destination, create another subprocess if needed.
	auto const &rname(occ.getRname());
	auto it(m_subprocesses.find(rname));
	if (m_subprocesses.end() == it)
	{
		auto const bam_name(boost::str(boost::format("%s/all_mapped.%s.bam") % m_output_prefix % rname));
		std::cerr << "Outputting to " << bam_name << ", using " << m_thread_count << " additional threads.\n";
		std::cout << bam_name << '\n';

		auto const res(m_subprocesses.emplace(
			std::piecewise_construct,
			std::forward_as_tuple(rname),
			std::forward_as_tuple(
				m_samtools_path,
				"view",
				"-@", std::to_string(m_thread_count),
				"-b",
				"-o", bam_name,
				m_samtools_group
			)
		));
		
		// Make sure that emplace actually added something.
		assert(res.second);

		// A new file was opened, output the headers.
		it = res.first;
		auto &stream(it->second.in);
		for (auto const &header : m_sam_headers)
			stream << header << '\n';
	}
	
	// Output the record.
	it->second.in << occ << '\n';
}


void SplittingBamOutputHandler::finish()
{
	// Close the pipes.
	for (auto &kv : m_subprocesses)
	{
		auto &sp(kv.second);
		sp.in << std::flush;
		sp.in.close();
		sp.in.pipe().close();
	}
	
	// Wait for the processes to finish.
	for (auto &kv : m_subprocesses)
	{
		kv.second.child.wait();
		auto const status(kv.second.child.exit_code());
		if (0 != status)
			std::cerr << "*** WARNING: child exited with status " << status << '\n';
	}
}
