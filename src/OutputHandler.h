/**
 * Copyright 2020, Tuukka Norri <tsnorri@iki.fi>
 * 
 * This file is part of CHIC aligner.
 * 
 * CHIC aligner is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * CHIC aligner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with CHIC aligner.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef CHIC_ALIGNER_OUTPUT_HANDLER_H
#define CHIC_ALIGNER_OUTPUT_HANDLER_H

#include <boost/process.hpp>
#include <vector>
#include <string>


class SamRecord; // Fwd.

// Output sink.
class OutputHandler
{
protected:
	std::vector <std::string>	m_sam_headers;
	std::uint32_t				m_max_edit_distance{UINT32_MAX};
	
public:
	OutputHandler() = default;
	
	void set_sam_headers(std::vector <std::string> const &headers) { m_sam_headers = headers; }
	void set_max_edit_distance(std::uint32_t dist) { m_max_edit_distance = dist; }
	
	virtual ~OutputHandler() {}
	virtual void prepare() = 0;
	virtual void output_record(SamRecord const &occ) = 0;
	virtual void finish() = 0;
};


// Write to std::cout.
class StdoutOutputHandler final : public OutputHandler
{
	void prepare() override;
	void output_record(SamRecord const &occ) override;
	void finish() override;
};


// Split by reference and write to BAM files.
class SplittingBamOutputHandler final : public OutputHandler
{
protected:
	struct subprocess
	{
		boost::process::opstream	in{};
		boost::process::child		child;
		
		template <typename ... t_args>
		explicit subprocess(t_args && ... args):
			in(),
			child(
				std::forward <t_args>(args)...,
				boost::process::std_in < in,
				boost::process::std_out > stderr, // Write all the subprocess’s output to stderr.
				boost::process::std_err > stderr  // For some reason boost::process did not seem to have an overload for operator> and std::ostream.
			)
		{
		}
	};
	
	typedef std::unordered_map <std::string, subprocess> subprocess_map;
	
protected:
	subprocess_map				m_subprocesses;
	boost::process::group		m_samtools_group;
	std::string					m_samtools_path;
	std::string					m_output_prefix;
	std::size_t					m_thread_count{};
	
public:
	SplittingBamOutputHandler() = default;
	
	SplittingBamOutputHandler(std::string samtools_path, std::string output_prefix, std::size_t thread_count):
		m_samtools_path(std::move(samtools_path)),
		m_output_prefix(std::move(output_prefix)),
		m_thread_count(thread_count)
	{
	}
	
	void prepare() override {} // No-op.
	void output_record(SamRecord const &occ) override;
	void finish() override;
};

#endif
