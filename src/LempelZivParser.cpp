/*
	 Copyright 2017, Daniel Valenzuela <dvalenzu@cs.helsinki.fi>

	 This file is part of CHIC aligner.

	 CHIC aligner is free software: you can redistribute it and/or modify
	 it under the terms of the GNU General Public License as published by
	 the Free Software Foundation, either version 3 of the License, or
	 (at your option) any later version.

	 CHIC aligner is distributed in the hope that it will be useful,
	 but WITHOUT ANY WARRANTY; without even the implied warranty of
	 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 GNU General Public License for more details.

	 You should have received a copy of the GNU General Public License
	 along with CHIC aligner.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "./LempelZivParser.h"
#include <vector>
#include <utility>
#include <iostream>
#include <string>
#include <algorithm>
#include "../ext/LZ/LZscan/algorithm/lzscan.h"
#include "../ext/ReLZ/src/io/async_vbyte_stream_reader.hpp"

namespace LempelZivParser {
void GetLZPhrases(std::vector<std::pair<uint64_t, uint64_t>> * lz_phrases_ptr, HybridLZIndex * HY) {
  uchar * tmp_seq = HY->GetTmpSeq();
  size_t text_len = HY->GetTextLength();
  char * text_filename = HY->GetTextFileName();
  LZMethod lz_method = HY->GetLZMethod();
  KernelType kernel_type = HY->GetKernelType();
  int max_memory_MB = HY->GetMaxMemoryMB();
  int rlz_ref_len_MB = HY->GetRLZRefLength();
  int n_threads = HY->GetNThreads();
  std::string lz_parse_tmp_filename = std::string(HY->GetTextFileName()) + ".tmp.lzparse";
  switch (lz_method) {
    case LZMethod::IN_MEMORY: {
      std::cerr << "IM" << std::endl;
      LempelZivParser::GetParseLZScan(tmp_seq, text_len, lz_phrases_ptr, max_memory_MB);
      LempelZivParser::SaveLZParse(lz_phrases_ptr, lz_parse_tmp_filename.c_str());
    }
    break;

    case LZMethod::EXTERNAL_MEMORY: {
      std::cerr << "EM" << std::endl;
      LempelZivParser::GetParseEM(text_filename, lz_parse_tmp_filename.c_str(), lz_phrases_ptr, max_memory_MB);
    }
    break;

    case LZMethod::RLZ: {
      std::cerr << "RLZ" << std::endl;
      if (rlz_ref_len_MB == 0) {
        // TODO: Should be better computed...
        rlz_ref_len_MB = std::min((size_t)1000, text_len);  // 1 gig as the SA can use 32bits ... but a better computation should be used
        std::cerr << "Warning: RLZ reference length was not specified. Using " << rlz_ref_len_MB << "MB" << std::endl;
      }
      LempelZivParser::GetParseReLZ(text_filename, kernel_type, lz_parse_tmp_filename.c_str(), lz_phrases_ptr, max_memory_MB, rlz_ref_len_MB, 0, n_threads);
    }
    break;
    
    case LZMethod::RELZ: {
      std::cerr << "RLZ" << std::endl;
      if (rlz_ref_len_MB == 0) {
        // TODO: Should be better computed...
        rlz_ref_len_MB = std::min((size_t)1000, text_len);  // 1 gig as the SA can use 32bits ... but a better computation should be used
        std::cerr << "Warning: RLZ reference length was not specified. Using " << rlz_ref_len_MB << "MB" << std::endl;
      }
      LempelZivParser::GetParseReLZ(text_filename, kernel_type, lz_parse_tmp_filename.c_str(), lz_phrases_ptr, max_memory_MB, rlz_ref_len_MB, 99, n_threads);
    }
    break;

    case LZMethod::INPUT_PLAIN: {
      std::cerr << "INPUT PLAIN LZ PARSE" << std::endl;
      FILE * lz_infile  = Utils::OpenReadOrDie(HY->GetInputLZFilename());
      LempelZivParser::LoadLZParsePlain(lz_infile, lz_phrases_ptr);
      fclose(lz_infile);
    }
    break;
    
    case LZMethod::INPUT_VBYTE: {  // TODO: needs to be tested.
      std::cerr << "INPUT PLAIN VBYTE PARSE" << std::endl;
      LempelZivParser::LoadLZParseVbyte(HY->GetInputLZFilename(), lz_phrases_ptr);
    }
    break;

    default:
    {
      std::cerr << "I don't know this LZ Construction method" << std::endl;
      exit(-1);
    }
  }
}
void LoadLZParsePlain(FILE * fp,
                 std::vector<std::pair<uint64_t, uint64_t>> * lz_phrases_ptr) {
  // We assume 64 bits per number
  fseek(fp, 0, SEEK_END);
  size_t file_len = (size_t)ftell(fp);
  fseek(fp, 0, SEEK_SET);
  // beware this len is the numbers of bytes.
  file_len = file_len*sizeof(uchar)/sizeof(uint64_t);
  ASSERT(file_len % 2 == 0);
  uint64_t * buff = new uint64_t[file_len];
  if (file_len != fread(buff, sizeof(uint64_t), file_len, fp)) {
    std::cerr << stderr << "Error reading string from file" << std::endl;
    exit(EXIT_FAILURE);
  }
  size_t n_phrases = file_len/2;

  lz_phrases_ptr->reserve(n_phrases);
  for (size_t i = 0; i < n_phrases; i++) {
    uint64_t pos = buff[2*i];
    uint64_t len = buff[2*i+1];
    lz_phrases_ptr->push_back(std::pair<uint64_t, uint64_t>(pos, len));
  }
  delete [] buff;
}

void LoadLZParseVbyte(std::string filename,
                 std::vector<std::pair<uint64_t, uint64_t>> * lz_phrases_ptr) {
  io_private::async_vbyte_stream_reader<uint64_t> reader;
  reader.open(filename);
  while (!reader.empty()) {
    uint64_t pos = reader.read();
    assert(!reader.empty());
    uint64_t len = reader.read();
    lz_phrases_ptr->push_back(std::pair<uint64_t, uint64_t>(pos, len));
  }
}

void SaveLZParse(std::vector<std::pair<uint64_t, uint64_t>> * lz_phrases_ptr,
                 std::string lzparse_filename) {
  // Current standard: 64 bits per number
  // TODO: skip the buffer and save directly.
  // Buffer was necessary when in memory we used 32 bits per len, now
  // what we keep in mem is same as in file, 64bits per each number.
  size_t n_phrases = lz_phrases_ptr->size();
  uint64_t * buff = new uint64_t[2*n_phrases];
  for (size_t i = 0; i < n_phrases; i++) {
    buff[2*i] = lz_phrases_ptr->at(i).first;
    buff[2*i + 1] = lz_phrases_ptr->at(i).second;
  }

  FILE * fp = Utils::OpenWriteOrDie(lzparse_filename.c_str());
  if (2*n_phrases != fwrite(buff, sizeof(uint64_t), 2*n_phrases, fp)) {
    std::cerr << "Error writing the LZ parse" << std::endl;
    exit(1);
  }

  fclose(fp);
  delete [] buff;
}

void GetParseEM(char * filename, std::string lzparse_filename,
                std::vector<std::pair<uint64_t, uint64_t>> * lz_phrases_ptr, int max_memory_MB) {
  std::string command_parse;
  command_parse += "emlz-parser " + std::string(filename);
  command_parse += " --mem=" + std::to_string(max_memory_MB);
  command_parse += " --output=" + lzparse_filename;
  command_parse += " >" + lzparse_filename + ".log_emlzparse 2>&1";
	// TODO: this is a namespace, we still do not have a verbose variable visible here.
  std::cerr << "-------------------------------------" << std::endl;
  std::cerr << "To obtain LZ parse we will call: " << command_parse << std::endl << std::endl;
  std::cerr << "-------------------------------------" << std::endl;
  if (system(command_parse.c_str())) {
    std::cerr << "Command failed. " << std::endl;
    exit(-1);
  }

  FILE *lz_infile = Utils::OpenReadOrDie(lzparse_filename.c_str());
  LempelZivParser::LoadLZParsePlain(lz_infile, lz_phrases_ptr);
  fclose(lz_infile);
}

void GetParseReLZ(char * filename,
                   KernelType kernel_type,
                   std::string lzparse_filename,
                   std::vector<std::pair<uint64_t, uint64_t>> * lz_phrases_ptr,
                   int max_memory_MB,
                   int rlz_ref_len_MB,
                   int maxD,
                   int n_threads) {
  std::string command_parse;
  command_parse += "ReLZ -o " + lzparse_filename;
  command_parse += " -e VBYTE";  // OUTPUT will be stored in Vbyte
  command_parse += " -d " + std::to_string(maxD);  // iterations without limit (in practice)
  command_parse += " -l " + std::to_string(rlz_ref_len_MB);
  command_parse += " -t " + std::to_string(n_threads);
  command_parse += " -m " + std::to_string(max_memory_MB);
  if (Utils::IsBioKernel(kernel_type)) {
    command_parse += " -f ";
  }
  command_parse += std::string(filename) + " > " + lzparse_filename + ".log_RLZparse 2>&1";
  // TODO: this is a namespace, we still do not have a verbose variable visible here.
  std::cerr << "-------------------------------------" << std::endl;
  std::cerr << "To obtain LZ parse we will call: " << command_parse << std::endl << std::endl;
  std::cerr << "-------------------------------------" << std::endl;
  if (system(command_parse.c_str())) {
    std::cerr << "Command failed. Catting log file:" << std::endl;
    std::string command_debug = "cat ";
    command_debug+= " " + lzparse_filename + ".log_RLZparse";
    if (system(command_debug.c_str())) {
      std::cerr << "Cat failed" << std::endl;
    }
    exit(-1);
  }

  LempelZivParser::LoadLZParseVbyte(lzparse_filename, lz_phrases_ptr);
}

void GetParseLZScan(uchar *seq,
                    size_t seq_len,
                    std::vector<std::pair<uint64_t, uint64_t>> * lz_phrases_ptr, int max_memory_MB) {
  std::vector<std::pair<int, int>> tmp_phrases;
  // TODO: ASSERT that texts fits in int, otherwise EM should be used !
	int  max_block_size = max_memory_MB << 20;
	if (max_block_size < 0) {
		max_block_size  = INT_MAX;
	}
  size_t n_phrases = (uint)parse(seq, (int)seq_len, max_block_size, &tmp_phrases);

  lz_phrases_ptr->reserve(n_phrases);
  for (size_t i = 0; i < n_phrases; i++) {
    uint64_t first  = (uint64_t)tmp_phrases.at(i).first;
    uint64_t second = (uint64_t)tmp_phrases.at(i).second;
    lz_phrases_ptr->push_back(std::pair<uint64_t, uint64_t>(first, second));
  }
}
}  // namespace LempelZivParser
