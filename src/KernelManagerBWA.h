/*
	 Copyright 2017, Daniel Valenzuela <dvalenzu@cs.helsinki.fi>

	 This file is part of CHIC aligner.

	 CHIC aligner is free software: you can redistribute it and/or modify
	 it under the terms of the GNU General Public License as published by
	 the Free Software Foundation, either version 3 of the License, or
	 (at your option) any later version.

	 CHIC aligner is distributed in the hope that it will be useful,
	 but WITHOUT ANY WARRANTY; without even the implied warranty of
	 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 GNU General Public License for more details.

	 You should have received a copy of the GNU General Public License
	 along with CHIC aligner.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef KERNELMANAGERBWA_H_
#define KERNELMANAGERBWA_H_

#include "utils.h"
#include "KernelManager.h"
#include <vector>
#include <sdsl/rmq_support.hpp>
#include <sdsl/util.hpp>
#include <sdsl/util.hpp>
#include <sdsl/io.hpp>
#include <sdsl/suffix_arrays.hpp>
#include <sdsl/bit_vectors.hpp>
#include <sdsl/rank_support.hpp>
#include <sdsl/select_support.hpp>
#include <sdsl/enc_vector.hpp>
#include <sdsl/coder_elias_delta.hpp>

class KernelManagerBWA : public KernelManager {
 public:
  // Index construction:
  KernelManagerBWA();
  KernelManagerBWA(uchar * text,
                   size_t text_len,
                   char * _kernel_text_filename,
                   int _verbose);
  void CreateKernelTextFile(uchar * kernel_text, size_t kernel_text_len);
  void DeleteKernelTextFile();
  void ComputeSize();
  virtual ~KernelManagerBWA();
  void SetFileNames(char * _prefix);
  void Load(char * _prefix, int n_threads, int verbose);
  void Save() const;
  size_t ComputeLength(); 


  // Queries and accessors:
  std::string LocateOccsFQ(char * query_filename,
                      char * mates_filename,
                      bool retrieve_all,
                      bool single_file_paired,
                      std::vector<std::string> kernel_options,
                      std::string const &tempdir) const;
  std::vector<SamRecord> LocateOccs(std::string query) const;
  void DetailedSpaceUssage() const;
  /*
  size_t GetLength() const {
    return kernel_text_len;
  }
  */
  static std::vector<SamRecord> SamOccurrences(const char * sam_filename);

 private:
  // Variables:
  size_t header_len;
  //size_t kernel_text_len;

  //char kernel_text_filename[200];
  std::string kernel_text_filename;
  // Own files:
  //char kernel_index_filename[200];
  std::string kernel_index_filename;
};

#endif /* KERNELMANAGERBWA_H__*/
