/*
	 Copyright 2017, Daniel Valenzuela <dvalenzu@cs.helsinki.fi>

	 This file is part of CHIC aligner.

	 CHIC aligner is free software: you can redistribute it and/or modify
	 it under the terms of the GNU General Public License as published by
	 the Free Software Foundation, either version 3 of the License, or
	 (at your option) any later version.

	 CHIC aligner is distributed in the hope that it will be useful,
	 but WITHOUT ANY WARRANTY; without even the implied warranty of
	 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 GNU General Public License for more details.

	 You should have received a copy of the GNU General Public License
	 along with CHIC aligner.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <getopt.h>
#include <sdsl/util.hpp>
#include <sdsl/vectors.hpp>
#include <algorithm>
#include <vector>
#include <string>
#include <fstream>
#include "./HybridLZIndex.h"
#include "OutputHandler.h"

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"


void suggest_help();
void suggest_help(char ** argv) {
  std::cerr << "For help, type " << argv[0] << " --help" << std::endl;
}

void print_help();
void print_help() {
  std::cerr << "Compressed Hybrid Index v0.1 beta" << std::endl;
  std::cerr << "chic_align aligns reads in fq format" << std::endl;
  std::cerr << std::endl;
  std::cerr << "Usage: chic_align [OPTIONS] <index_basename> <reads1.fq> (reads2.fq)" << std::endl;
  std::cerr << std::endl;
  std::cerr << "Options:" << std::endl;
  std::cerr << "-s --secondary-report=[ALL|LZ|NONE] Default=NONE" << std::endl;
  std::cerr << "-t --threads=(number of threads)" << std::endl;
  std::cerr << "-e --max-ed=DISTANCE " << std::endl;
  std::cerr << "-p --interleaved-reads " << std::endl;
  std::cerr << "-r --split-output-by-reference " << std::endl;
  std::cerr << "-P --output-prefix" << std::endl;
  std::cerr << "-K --kernel-options " << std::endl;
  std::cerr << "-S --samtools-path=PATH" << std::endl;
  std::cerr << "-v --verbose=LEVEL " << std::endl;
  std::cerr << "--help " << std::endl;
}

typedef struct {
  // Required:
  char * index_basename;
  char * patterns_filename;
  // Options:
  char * mates_filename{};
  bool interleaved_mates{};
  InputType input_type{InputType::FQ};
  SecondaryReportType secondary_report{SecondaryReportType::NONE};
  std::string samtools_path{"samtools"};
  std::string output_prefix{"."};
  int n_threads{1};
  int verbose{1};
  std::uint32_t max_edit_distance{UINT32_MAX};
  std::vector<std::string> kernel_options;
  bool split_output_by_reference{};
  bool use_samtools_threads{true};
} Parameters;


int global_correct = 0;
void FailExit();
void Success();

void FailExit() {
  printf(ANSI_COLOR_RED "\tTest Failed\n\n" ANSI_COLOR_RESET);
  exit(1);
}
void Success() {
  printf(ANSI_COLOR_BLUE "\t\tSuccess\n" ANSI_COLOR_RESET);
}

int main(int argc, char *argv[]) {
  Parameters * parameters  = new Parameters();
  while (1) {
    static struct option long_options[] = {
      /* These options don’t set a flag.
         We distinguish them by their indices. */
      {"secondary-report",          required_argument, 0, 's'},
      {"threads",                   required_argument, 0, 't'},
      {"verbose",                   required_argument, 0, 'v'},
      {"max-ed",                    required_argument, 0, 'e'},
      {"interleaved-reads",         no_argument,       0, 'p'},
      {"split-output-by-reference", no_argument,       0, 'r'},
      {"output-prefix",             required_argument, 0, 'P'},
      {"samtools-path",             required_argument, 0, 'S'},
      {"no-samtools-threads",       no_argument,       0, 'T'},
      {"kernel-options",            required_argument, 0, 'K'},
      {"help",                      no_argument,       0, 'h'},
      {0, 0, 0, 0}
    };
    /* getopt_long stores the option index here. */
    int option_index = 0;

    int c = getopt_long(argc, argv, "s:t:v:e:p:r:P:S:T:K:h", long_options, &option_index);

    /* Detect the end of the options. */
    if (c == -1)
      break;

    switch (c) {
      case 0:
        /* If this option set a flag, do nothing else now. */
        if (long_options[option_index].flag != 0)
          break;
        printf("option %s", long_options[option_index].name);
        if (optarg)
          printf(" with arg %s", optarg);
        printf("\n");
        break;

      case 'h':
        print_help();
        delete (parameters);
        exit(0);
        break;

      case 'p':
        parameters->interleaved_mates = true;
        break;

      case 's':
        if (strcmp(optarg, "ALL") == 0) {
          parameters->secondary_report = SecondaryReportType::ALL;
        } else if (strcmp(optarg, "LZ") == 0) {
          parameters->secondary_report = SecondaryReportType::LZ;;
        } else if (strcmp(optarg, "NONE") == 0) {
          parameters->secondary_report = SecondaryReportType::NONE;;
        } else {
          print_help();
          suggest_help(argv);
          delete (parameters);
          exit(-1);
        }
        break;
      
      case 'e':
        parameters->max_edit_distance = atoi(optarg);
        break;
      
      case 'r':
        parameters->split_output_by_reference = true;
        break;
      
      case 'P':
        parameters->output_prefix = std::string(optarg);
        break;
      
      case 'S':
        parameters->samtools_path = std::string(optarg);
        break;
      
      case 'T':
        parameters->use_samtools_threads = false;
        break;

      case 'K':
        parameters->kernel_options.push_back(std::string(optarg));
        break;

      case 't':
        parameters->n_threads = atoi(optarg);
        break;

      case 'v':
        parameters->verbose = atoi(optarg);
        break;

      case '?':
        /* getopt_long already printed an error message. */
        suggest_help(argv);
        exit(-1);
        break;

      default:
        suggest_help(argv);
        exit(-1);
    }
  }

  int rest = argc - optind;
  if (rest != 2 && rest != 3) {
    std::cerr << "Incorrect number of arguments." << std::endl;
    suggest_help(argv);
    exit(-1);
  }
  parameters->index_basename = argv[optind];
  parameters->patterns_filename= argv[optind + 1];
  if (rest == 3) {
    parameters->mates_filename = argv[optind + 2];
  }
  //////////////////////////////////////
  // TODO: Sanitize parameters: e,g, -output=file.out
  // fails... it should be --output=file.out
  // or -o file.out
  ///////////////////////////////////////
  long double t1, t2;

  t1 = Utils::wclock();

  // Create a temporary directory.
  std::string tempdir(parameters->output_prefix);
  tempdir += "/temp.XXXXXX";
  // mkdtemp will modify the buffer.
  if (!mkdtemp(tempdir.data()))
  {
	  std::cerr << "ERROR: Unable to create a temporary directory with template “" << tempdir << "”: " << strerror(errno) << '\n';
	  std::exit(1);
  }
  
  HybridLZIndex * my_index = new HybridLZIndex();
  my_index->Load(parameters->index_basename,
                 parameters->n_threads,
                 parameters->verbose);

  std::cerr << "LZ Index succesfully loaded" << std::endl;
  std::cerr << "Secondary report type: " << to_string(parameters->secondary_report) << '\n';
  
  std::unique_ptr <OutputHandler> output_handler;
  if (parameters->split_output_by_reference)
  {
    output_handler.reset(new SplittingBamOutputHandler(
      parameters->samtools_path,
      parameters->output_prefix,
      parameters->use_samtools_threads ? 2 : 0
    ));
  }
  else
  {
    output_handler.reset(new StdoutOutputHandler());
  }
  output_handler->set_max_edit_distance(parameters->max_edit_distance);
  
  my_index->FindFQ(parameters->patterns_filename,
                   parameters->mates_filename,
                   parameters->interleaved_mates,
                   parameters->secondary_report,
                   parameters->kernel_options,
                   tempdir,
                   *output_handler);

  t2 = Utils::wclock();
  std::cerr << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << std::endl;
  std::cerr << "Reads aligned in: "<< (t2-t1) << " seconds. " << std::endl;
  if (t2-t1 > 60) {
    std::cerr << "Reads aligned in: "<< (t2-t1)/60 << " minutes. " << std::endl;
  }
  if (t2-t1 > 3600) {
    std::cerr << "Reads aligned in: "<< (t2-t1)/3600 << " hours. " << std::endl;
  }
  std::cerr << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << std::endl;

  delete(my_index);
  delete(parameters);
  return 0;
}

void ValidatePatterns(std::vector<std::string> patterns, size_t max_len) {
  // TODO: this code should be compiled conditionally, perhaps using DNDEBUG
  for (size_t i = 0; i < patterns.size(); i++) {
    if (patterns[i].size() > max_len) {
      std::cerr << "Patterns are larger than the index limit:" << std::endl;
      std::cerr << "Limit: " << max_len << std::endl;
      std::cerr << "Offending pattern:" << patterns[i] << std::endl;

      exit(EXIT_FAILURE);
    }
  }
}
