/**
 * Copyright 2020, Tuukka Norri <tsnorri@iki.fi>
 * 
 * This file is part of CHIC aligner.
 * 
 * CHIC aligner is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * CHIC aligner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with CHIC aligner.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <cassert>
#include <iostream>
#include "SamRecord.h"


void SamRecord::report_unable_to_parse() const
{
	std::cerr << "*** WARNING: unable to parse SAM record: '" << m_rec << "'\n";
}


void SamRecord::reset()
{
	m_optional_fields.clear();
	m_qname.clear();
	m_rname.clear();
	m_cigar.clear();
	m_rnext.clear();
	m_seq.clear();
	m_qual.clear();
	m_length = 0;
	m_pos = 0;
	m_pnext = 0;
	m_tlen = 0;
	m_flags = 0;
	m_edit_distance = 0;
	m_mapq = 0;
}


void SamRecord::parse()
{
	assert(m_optional_fields.empty());

	std::string_view const &rec_sv(m_rec);
	std::size_t start(0);
	
	// Mandatory fields.
	std::size_t field_idx(0);
	while (field_idx < 10)
	{
		auto const tab_pos(rec_sv.find('\t', start));
		if (std::string::npos == tab_pos)
			break;
		
		switch (field_idx)
		{
			case 0:		// QNAME
				m_qname = rec_sv.substr(start, tab_pos - start);
				break;
			case 1:		// FLAG
				if (!parse_integer(rec_sv.substr(start, tab_pos - start), m_flags))
					goto end_loop;
				break;
			case 2:		// RNAME
				m_rname = rec_sv.substr(start, tab_pos - start);
				break;
			case 3:		// POS
				if (!parse_integer(rec_sv.substr(start, tab_pos - start), m_pos))
					goto end_loop;
				if (0 == m_pos && ! (0x4 & m_flags)) // Should be unmapped if POS is 0.
					goto end_loop;
				break;
			case 4:		// MAPQ
				if (!parse_integer(rec_sv.substr(start, tab_pos - start), m_mapq))
					goto end_loop;
				break;
			case 5:		// CIGAR
				m_cigar = rec_sv.substr(start, tab_pos - start);
				break;
			case 6:		// RNEXT
				m_rnext = rec_sv.substr(start, tab_pos - start);
				break;
			case 7:		// PNEXT
				if (!parse_integer(rec_sv.substr(start, tab_pos - start), m_pnext))
					goto end_loop;
				break;
			case 8:		// TLEN
				if (!parse_integer(rec_sv.substr(start, tab_pos - start), m_tlen))
					goto end_loop;
				break;
			case 9:		// SEQ
				m_seq = rec_sv.substr(start, tab_pos - start);
				break;
		}
		
		start = 1 + tab_pos;
		++field_idx;
	}
	
	end_loop:
	if (field_idx != 10)
		report_unable_to_parse();
	else
	{
		// QUAL and Optional fields.
		auto const tab_pos(rec_sv.find('\t', start));
		if (std::string::npos == tab_pos)
		{
			// No optional fields.
			m_qual = rec_sv.substr(start);
		}
		else
		{
			m_qual = rec_sv.substr(start, tab_pos - start);
			
			// Optional fields.
			start = 1 + tab_pos;
			while (true)
			{
				// Identifier.
				auto const colon1_pos(rec_sv.find(':', start));
				if (std::string::npos == colon1_pos)
				{
					report_unable_to_parse();
					break;
				}
				auto const opt_identifier(rec_sv.substr(start, colon1_pos - start));
				start = 1 + colon1_pos;
				
				auto const colon2_pos(rec_sv.find(':', start));
				if (std::string::npos == colon2_pos)
				{
					report_unable_to_parse();
					break;
				}
				char const opt_type(rec_sv[start]);
				start = 1 + colon2_pos;
				
				auto const tab_pos(rec_sv.find('\t', start));
				auto const val(rec_sv.substr(start, tab_pos - start)); // Handles potential npos.
				start = 1 + tab_pos;
				
				m_optional_fields.emplace_back(std::string(opt_identifier), std::string(val), opt_type);
				
				// Handle edit distance.
				if ("NM" == opt_identifier)
					parse_integer(val, m_edit_distance);
				
				// Check if this was the last optional field.
				if (std::string::npos == tab_pos)
					break;
			}
		}
	}
	
	m_length = lengthFromCigar();
	
	// Remove b.c. the values may be changed by calling accessors.
	m_rec.clear();
}


std::size_t SamRecord::lengthFromCigar() const
{
	if (0 == m_pos || "*" == m_rname)
		return 0;
	
	std::size_t retval(0);
	std::size_t current_count(0);
	for (auto const c : m_cigar)
	{
		if (isdigit(c))
		{
			current_count *= 10;
			current_count += c - 48;
		}
		else
		{
			switch (c)
			{
				case 'M': // Match or mismatch
				case 'I': // Insertion
				case 'X': // Mismatch
				case '=': // Match
					retval += current_count;
					break;
				default:
					break;
			}
			current_count = 0;
		}
	}
	return retval;
}


std::ostream &operator<<(std::ostream &os, SamRecord const &rec)
{
	assert(rec.m_rec.empty());
	os << rec.m_qname << '\t' << rec.m_flags << '\t' << rec.m_rname << '\t' << rec.m_pos << '\t'
		<< rec.m_mapq << '\t' << rec.m_cigar << '\t' << rec.m_rnext << '\t' << rec.m_pnext << '\t'
		<< rec.m_tlen << '\t' << rec.m_seq << '\t' << rec.m_qual;
	
	for (auto const &opt : rec.m_optional_fields)
		os << '\t' << opt.name << ':' << opt.type << ':' << opt.text;
	
	return os;
}
