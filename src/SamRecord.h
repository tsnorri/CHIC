/**
 * Copyright 2020, Tuukka Norri <tsnorri@iki.fi>
 * 
 * This file is part of CHIC aligner.
 * 
 * CHIC aligner is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * CHIC aligner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with CHIC aligner.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef CHIC_ALIGNER_SAM_RECORD_H
#define CHIC_ALIGNER_SAM_RECORD_H

#include <boost/spirit/home/x3.hpp>
#include <string>
#include <string_view>
#include <vector>


class SamRecord
{
	friend std::ostream &operator<<(std::ostream &os, SamRecord const &rec);
	
public:
	struct optional_field
	{
		std::string	name;
		std::string	text;
		char type{};
		
		optional_field() = default;
		
		optional_field(std::string name_, std::string text_, char type_):
			name(std::move(name_)),
			text(std::move(text_)),
			type(type_)
		{
		}
	};
	
	typedef std::vector <optional_field> optional_field_vector;
	
protected:
	optional_field_vector	m_optional_fields;
	std::string				m_rec;		// Unparsed.
	std::string				m_qname;
	std::string				m_rname;
	std::string				m_cigar;
	std::string				m_rnext;
	std::string				m_seq;
	std::string				m_qual;
	std::size_t				m_length{};
	std::uint64_t			m_pos{};	// 1-based.
	std::uint64_t			m_pnext{};
	std::uint64_t			m_tlen{};
	std::uint32_t			m_flags{};
	std::uint32_t			m_edit_distance{};
	std::int32_t			m_mapq{};
	
public:
	SamRecord() = default;
	
	explicit SamRecord(std::string const &rec):
		m_rec(rec)
	{
	}
	
	// FIXME: remove this constructor.
	SamRecord(std::uint64_t pos, std::uint64_t len):
		m_length(len),
		m_pos(1 + pos)
	{
	}
	
	void parse();
	void reset();
	
	std::string const &getRec() const { return m_rec; }
	std::string const &getQname() const { return m_qname; }
	std::string const &getRname() const { return m_rname; }
	std::uint64_t getPos() const { return m_pos; }
	std::uint64_t getZeroBasedPos() const { return m_pos - 1; }
	std::size_t getLength() const { return m_length; }
	std::uint32_t getEditDistance() const { return m_edit_distance; }
	bool isUnmapped() const { return (0x4 & m_flags ? true : false); }
	
	void setRecord(std::string str) { reset(); m_rec = str; }
	void setZeroBasedPos(std::uint64_t pos) { m_pos = 1 + pos; }
	void setRname(std::string rname) { m_rname = std::move(rname); }
	void setFlags(std::uint32_t flags) { m_flags = flags; }
	
protected:
	void report_unable_to_parse() const;
	
	template <typename t_dst>
	bool parse_integer(std::string_view const &str, t_dst &dst)
	{
		//auto const res(std::from_chars(str.data(), str.data() + str.size(), dst));
		//return (std::errc() == res.ec);
		static constexpr boost::spirit::x3::int_parser <t_dst, 10> parser{};
		return boost::spirit::x3::parse(str.begin(), str.end(), parser, dst);
	}
	
	std::size_t lengthFromCigar() const;
};

#endif
