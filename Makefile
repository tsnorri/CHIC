all: ext/ReLZ/common.mk
	$(MAKE) -C ext
	$(MAKE) -C src

clean: ext/ReLZ/common.mk
	$(MAKE) -C ext clean
	$(MAKE) -C src clean

ext/ReLZ/common.mk: common.mk
	cp $< $@
